<?php
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);


require_once("auth.php");



session_start();
$cart = &$_SESSION["cart"];
$quantity = $_POST["quantity"];
$id = intval($_POST["id"]);



if (array_key_exists($id, $cart)) {
	$cart[$id] += $quantity;
} else {
	$cart[$id] = $quantity;
}

header("Location: cart.php");
?>	
