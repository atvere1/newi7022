



<?php
require_once("auth.php");
require_once("header.php");
require_once("database.php");
require_once("common.php");

$query="select * from product";
$statement = $db->prepare($query);
$statement->execute();

$products_sql = $statement->fetchAll(PDO::FETCH_ASSOC); 
$products = [];

foreach ($products_sql as &$product) {
	$products[$product['id']] = $product;
}



$table="";
$html =
   '<tr>' .
   '<td><a href="product.php?id={{id}}">{{name}}</a></td>' .
   '<td>{{quantity}}</td>' .
   '<td>{{price}}<span class="glyphicon glyphicon-bitcoin" aria-hidden="true"></span></td>' .
   '<td>{{total}}<span class="glyphicon glyphicon-bitcoin" aria-hidden="true"></span></td>' .
   '</tr>';
$grandTotal= 0;

   
foreach ($cart as $id => $quantity) {
	$table .= template($html, [
		'id' => $id,
		'name' => $products[$id]['name'] ,
		'price' => $products[$id]['price'],
		'quantity' => $quantity,
		'total' => $products[$id]['price']*$quantity
	]);
	$grandTotal+= $products[$id]['price']*$quantity;
}


?>
<div class="alert alert-success" role="alert">
  <button type="button"	 class="alert-link" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Successfully added product to cart.
</div>	


<!-- <div class="alert alert-success" role="alert">You did something to warrant this message.</div> -->
<h2>Cart</h2>

<table>
  <tr>
    <th>Name</th>
    <th>Quantity</th>
    <th>Price</th>
    <th>Total</th>
  </tr>
<?=$table?>

</table>

<h1>Your grand total is <?=$grandTotal?><span class="glyphicon glyphicon-bitcoin" aria-hidden="true"></span></h1>
    <form class="navbar-form navbar-left" action="Buyout.php" method="post">
			<div class="form-group">
			<button type="submit" class="btn btn-default">Place Order</button>

		  </form>
</body>
</html>

