<?php

require_once("header.php");
require_once("database.php");
require_once("auth.php");
require_once("common.php");

$query="select * from product where name like :name";

$text='%'.$_POST["searchValue"].'%';

$statement = $db->prepare($query);
$statement->bindParam("name",$text);

$statement->execute();


$searchedProducts=$statement->fetchAll(PDO::FETCH_ASSOC); 



$table="";
$html =
   '<tr>' .
   '<td><a href="product.php?id={{id}}">{{name}}</a></td>' .
   '<td>{{price}}<span class="glyphicon glyphicon-bitcoin" aria-hidden="true"></span></p></td>' .
   '</tr>';
   
foreach ($searchedProducts as $product){
	$table .= template($html, [
		'id' => $product['id'],
		'name' => $product['name'],
		'price' => $product['price']
	]);
}
   
if(count($searchedProducts)===0){
	echo "0 Matches found";
	exit;
}

?>
 <h2>Search Results</h2>
<table>
  <tr>

    <th>Name</th>
    <th>Price</th>
    <th></th>
  </tr>
<?=$table?>
</table>
