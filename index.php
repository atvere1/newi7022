<?php 
require_once("variables.php");
require_once("database.php");
require_once("header.php");
﻿?>
<h1>Log in to see content</h1>

<div class="container">
	<h2 class="login-title">- Please Login -</h2>
	<div class="panel panel-default container" style="width: 30%">
		<div class="panel-body" >
			<form action="login.php" method="post">
				<div class="input-group login-userinput">
					<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
					<input id="txtUser" type="email" class="form-control" name="email" placeholder="Username">
				</div>
				<div class="input-group">
					<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
					<input  id="txtPassword" type="password" class="form-control" name="password" placeholder="Password">
					<span id="showPassword" class="input-group-btn">
            <button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
          </span>  
				</div>
				<button class="btn btn-primary btn-block login-button" type="submit"><i class="fa fa-sign-in"></i> Login</button>
				<div class="checkbox login-options">
					<label><input type="checkbox"/> Remember Me</label>
					<a href="registration.php" class="login-forgot">Register</a>
					<br></br>
					<a href="passwordResetForm.php" class="login-forgot">Forgot Password</a>
				</div>		
			</form>			
		</div>
	</div>
</div>

		     
    </body>
</html>

