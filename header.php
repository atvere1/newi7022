<?php
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);


if(!isset($_SESSION)){
	session_start();
}


if (!array_key_exists("cart", $_SESSION)) {
    $_SESSION["cart"] = [];
}
$cart = $_SESSION["cart"];

$uri = $_SERVER["REQUEST_URI"];


$product = $uri === "/products.php";
$about = $uri === "/about.php";


?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="description" content="Welcome to the shop of illegal goods">
    <title>www.apple.com</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
    <link href="css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <meta name="viewport" content="width=device-width, user-scalable=no"/><!-- Disable zoom on smartphone -->
  </head>
  <body>
	  <nav class="navbar navbar-default">
	    <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Crematorium</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav">
				<li <?php if($about){echo 'class="active"';}?>><a href="about.php">About</a></li>
			<li <?php if($product){echo 'class="active"';}?>> <a href="products.php">Products</a></li>
		  </ul>

		  <form class="navbar-form navbar-left" action="search.php" method="post">
			<div class="form-group">
			  <input type="search" class="form-control" name="searchValue" placeholder="Search">
			</div>
			<button type="submit" class="btn btn-default">Search</button>
		  </form>
		  
		  <ul class="nav navbar-nav navbar-right">
			<li><a href="cart.php"><span id="NavBarBuy" class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span></a></li>
		  </ul>
		  <?php
		  if(array_key_exists("user", $_SESSION)){
		  echo '<form class="navbar-form navbar-left" action="logout.php" method="post">';
          
		  echo'<button type="submit" class="btn btn-default">Logout</button>';
		  
	      }?>
		  </form>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->

	  </nav>




