<?phprequire_once("auth.php");


require_once("header.php");
require_once("common.php");
require_once("database.php");
?>

<!DOCTYPE html>



  <head>
    <meta charset="utf-8">
    <meta name="description" content="Welcome to the shop of illegal goods">
    <title>www.apple.com</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
    <link href="css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <meta name="viewport" content="width=device-width, user-scalable=no"/><!-- Disable zoom on smartphone -->
  </head>
  <body>
	  <nav class="navbar navbar-default">
	    <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Crematorium</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav">
				<li <?php if($about){echo 'class="active"';}?>><a href="about.php">About</a></li>
			<li <?php if($product){echo 'class="active"';}?>> <a href="products.php">Products</a></li>
		  </ul>

		  <form class="navbar-form navbar-left" action="search.php" method="post">
			<div class="form-group">
			  <input type="search" class="form-control" name="searchValue" placeholder="Search">
			</div>
			<button type="submit" class="btn btn-default">Search</button>
		  </form>
		  
		  <ul class="nav navbar-nav navbar-right">
			<li><a href="cart.php"><span id="NavBarBuy" class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span></a></li>
		  </ul>
		  <?php
		  if(array_key_exists("user", $_SESSION)){
		  echo '<form class="navbar-form navbar-left" action="logout.php" method="post">';
          
		  echo'<button type="submit" class="btn btn-default">Logout</button>';
		  
	      }?>
		  </form>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->

	  </nav>
<!--[if lt IE 7 ]><html lang="en-US" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#" class="no-js ie ie6 lte7 lte8 lte9 control-height"><![endif]-->
<!--[if IE 7 ]><html lang="en-US" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#" class="no-js ie ie7 lte7 lte8 lte9 control-height"><![endif]-->
<!--[if IE 8 ]><html lang="en-US" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#" class="no-js ie ie8 lte8 lte9 control-height"><![endif]-->
<!--[if IE 9 ]><html lang="en-US" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#" class="no-js ie ie9 lte9 control-height"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en-US" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#" class="no-js control-height lazy-loading"><!--<![endif]-->
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width" />
	
		
		<title>
			About | Cultivated Wit		</title>
		
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="shortcut icon" href="http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-child/favicon.ico" />
		<link rel="stylesheet" type="text/css" media="all" href="http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-child/style.css" />
		<link rel="pingback" href="http://www.cultivatedwit.com/xmlrpc.php" />

		<link rel="stylesheet" type="text/css" media="all" href="http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-child/css/jquery.fancybox.css" />
		

		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		
		<script type='text/javascript' src='http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-child/js/jquery.animate-shadow-min.js'></script>
		<script type='text/javascript' src='http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-child/js/jquery.hoverflow.min.js'></script>

		<script type='text/javascript' src='http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-child/js/jquery.ba-urlinternal.js'></script>
		<script type='text/javascript' src='http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-child/js/jquery.history.js'></script>
		<script type="text/javascript" src="http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-child/js/modernizr.custom.57081.js"></script>
		<script type="text/javascript" src="http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-child/js/css3-mediaqueries.js"></script>
		<script type="text/javascript" src="http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-child/js/jquery.scrollTo-1.4.2-min.js"></script>
		<script type='text/javascript' src='http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-child/js/jquery.fancybox.js'></script>
		<script type='text/javascript' src='http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-child/js/jquery.fitvids.js'></script>
		<script type='text/javascript' src='http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-child/js/imagesloaded.pkgd.min.js'></script>
		<script type='text/javascript' src='http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-child/js/jquery.widowFix-1.3.2.min.js'></script>	
		<script type='text/javascript' src='http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-child/js/jquery.lettering.js'></script>
		<script type='text/javascript' src='http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-child/js/jquery.fittext.js'></script>	
		<script type='text/javascript' src='http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-child/js/TweenMax.min.js'></script>	
		<script type='text/javascript' src='http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-child/js/jquery.superscrollorama.js'></script>
		<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/jquery-color/2.1.2/jquery.color.min.js'></script>		
		

		<script type="text/javascript" src="//use.typekit.net/oxa0wsk.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
		
		
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-36072146-5', 'auto');
	  ga('send', 'pageview');

	</script>

		
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Cultivated Wit &raquo; Feed" href="http://www.cultivatedwit.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="Cultivated Wit &raquo; Comments Feed" href="http://www.cultivatedwit.com/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/www.cultivatedwit.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.7.5"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<!--[if lt IE 9]>
	<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<script>window.html5 || document.write(unescape('%3Cscript src="http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-parent/js/ieshiv.js"%3E%3C/script%3E'))</script>
<![endif]-->
<link rel='stylesheet' id='contact-form-7-css'  href='http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=3.7.1' type='text/css' media='all' />
<script type='text/javascript' src='http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<link rel='https://api.w.org/' href='http://www.cultivatedwit.com/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.cultivatedwit.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.cultivatedwit.com/wp-includes/wlwmanifest.xml" /> 

<link rel="canonical" href="http://www.cultivatedwit.com/about/" />
<link rel='shortlink' href='http://www.cultivatedwit.com/?p=1408' />
<link rel="alternate" type="application/json+oembed" href="http://www.cultivatedwit.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.cultivatedwit.com%2Fabout%2F" />
<link rel="alternate" type="text/xml+oembed" href="http://www.cultivatedwit.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.cultivatedwit.com%2Fabout%2F&#038;format=xml" />

<script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '']);
_gaq.push(['_trackPageview']);
(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<meta property="og:title" name="og:title" content="About" />
<meta property="og:type" name="og:type" content="article" />
<meta property="og:image" name="og:image" content="http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/uploads/2013/06/Cultivated-Wit-edit-1024x452.png" />
<meta property="og:image" name="og:image" content="http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/uploads/2013/06/cropped-group-shot-300x100.jpg" />
<meta property="og:image" name="og:image" content="http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/uploads/2013/06/Cultivated-Wit_-51-1024x343.jpg" />
<meta property="og:url" name="og:url" content="http://www.cultivatedwit.com/about/" />
<meta property="og:description" name="og:description" content="We serve as an illegal store-front, a middle-man between army supplies and the standard gun-toting civilian. We sell guns to anyone with money, no restrictions are in place. Currently payment is accepted only in bitcoin but will branch out to phones too." />
<meta property="og:locale" name="og:locale" content="en_US" />
<meta property="og:site_name" name="og:site_name" content="Cultivated Wit" />
<meta property="twitter:card" name="twitter:card" content="summary" />
<meta property="article:published_time" name="article:published_time" content="2013-06-25T20:21:20+00:00" />
<meta property="article:author:first_name" name="article:author:first_name" content="Cultivated" />
<meta property="article:author:last_name" name="article:author:last_name" content="Wit" />
<meta property="article:author:username" name="article:author:username" content="cultivatedwit" />
	</head>
	<body class="page-template page-template-page-about page-template-page-about-php page page-id-1408 control-height">

		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=704729732885937";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		
		<!-- DO NOT REMOVE.  This is a link to the blog's homepage url. We access this in the javascript. If the wordpress blog URL changes the javascript immediately get updated.  Used in Ajax loading of all posts and pages -->
		<a href="http://www.cultivatedwit.com" id="blogURL" style="display:none"></a>
		<!-- DO NOT REMOVE.-->
		
		<div id="search-container" >
			<div id="search-inner" class="wrap container">
				<div id="close-button">
					<a class="close-button" onclick="event.preventDefault();" href="#close"></a>
				</div>

				<!--
<form role="search" method="get" id="searchform" action="http://www.cultivatedwit.com/">
    <div>
        <input type="text" value="" name="s" id="s" placeholder="search" />
    </div>
</form>
-->


<div id="clue-text-container">
	<div id="clue-text">
		You found a clue!
	</div>
	<img src="http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-child/img/hand_print.png" id="search-clue-image"/>
</div>				
			</div>
		</div>
		
		<div id="follow-container" class="">
	<div id="follow-inner" class=" container wrap">
		<div id="close-button">
			<a class="close-button" onclick="event.preventDefault();" href="#close"></a>
		</div>
		<div id="social-media-follow" class=" grid5 float-grid-item">
			
			<div class="follow-transparency-container clear">
				<h2 id="follow-greeting-1" class="follow-heading">
					Follow Our Followables
				</h2>
				<div id="follow-facebook" class="follow-row float-grid-item">
					<a class="follow-icon-link" href="http://facebook.com/cultivatedwit" title="Cultivated Wit on Facebook" target="_blank">
						<div id="facebook-follow-icon" class="main-follow-icon"></div>
					</a>
					<div class="fb-like grid1 float-grid-item" data-href="https://www.facebook.com/CultivatedWit" data-width="90" data-layout="button_count" data-show-faces="false" data-send="false"></div>
				</div>
		
				<div id="follow-twitter" class="follow-row float-grid-item">
					<a class="follow-icon-link" href="https://twitter.com/cultivatedwit" target="_blank">
						<div id="twitter-follow-icon" class="main-follow-icon grid1 float-grid-item"></div>
					</a>
						<a href="https://twitter.com/cultivatedwit" class="twitter-follow-button grid1 float-grid-item" data-show-count="false" data-show-screen-name="false" data-dnt="true">Follow @cultivatedwit</a>
								<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
						
				</div>
		
				<div id="follow-google" class="follow-row  float-grid-item last-float-item">
					<a class="follow-icon-link" href="http://www.youtube.com/user/CultivatedWit" target="_blank">
						<div id="google-follow-icon" class="main-follow-icon grid1 float-grid-item"></div>
					</a>
					<script src="https://apis.google.com/js/plusone.js"></script>

					<div class="g-ytsubscribe" data-channel="CultivatedWit" data-layout="default"></div>
				</div>
			</div>
			
		
		</div>
		
		<div class="follow-vertical-border-container grid1 float-grid-item">
			<div class="follow-vertical-border border-1"></div>
		</div>
		
		<div id="follow-whiskey" class="follow-row grid6 float-grid-item last-float-item clear">
			
			<div class="follow-transparency-container clear">
				<h2 id="follow-greeting-1" class="follow-heading">
					Whiskey Friday
				</h2>
				<a href="http://www.cultivatedwit.com/whiskey-friday/" class="whiskey-friday-link" >
					<div id="whiskey-follow-icon" class="main-follow-icon"></div>
					<div id="whiskey-friday-text-2">
						Let’s Drink
					</div> 
				</a>
			</div>
		</div>
		
		<div class="clear"></div>
		
		<div class="follow-horizontal-border-container clear">
			<div class="follow-horizontal-border"></div>
		</div>
			
		<div id="follow-newsletter" class="follow-row grid5 float-grid-item">

			<div class="follow-transparency-container clear">
				<h2 id="follow-greeting-3" class="follow-heading">
					Receive Our Newsletter
				</h2>
			
				<div id="newsletter-icon" class="main-follow-icon"></div> 
				<div class="wpcf7" id="wpcf7-f1863-t1-o1">
					<form action="/form-test/#wpcf7-f1863-t1-o1" method="post" class="wpcf7-form" novalidate="novalidate">
						<div style="display: none;">
							<input type="hidden" name="_wpcf7" value="1863" />
							<input type="hidden" name="_wpcf7_version" value="3.5.2" />
							<input type="hidden" name="_wpcf7_locale" value="en_US" />
							<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f1863-t1-o1" />
							<input type="hidden" name="_wpnonce" value="6c10e098ce" />
						</div>
						<span class="wpcf7-form-control-wrap your-email">
							<input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-email treat" id="newsletter-follow-input" placeholder="Your Email" />
						</span>
						<div class="wpcf7-response-output wpcf7-display-none"></div>
					</form>
				</div>
		
			</div>
			
		</div>
		
		
		<div class="follow-vertical-border-container grid1 float-grid-item">
			<div class="follow-vertical-border border-2"></div>
		</div>
		
		
		<div id="follow-email" class="follow-row follow-row grid6 float-grid-item last-float-item">
		
			<div class="follow-transparency-container clear">
				<h2 id="follow-greeting-4" class="follow-heading">
					Contact Us
				</h2>
				<div id="email-contact-icon" class="main-follow-icon"></div>
		
			
				<div id="follow-email-link">
					<a href="mailto:AnsweringMachine@CultivatedWit.com" target="_blank" title="Email Cultivated Wit">AnsweringMachine<br>@CultivatedWit.com</a>
				</div>
			</div>
		</div>

	</div>
</div>		
		<div id="header-container" >
			
					
					<div id="mobile-menu-button-container">
						<div id="mobile-menu-button"></div>
						<div id="mobile-menu-close-button"></div>
					</div>
					
				</header>
				<div id="header-mobile-shadow" class="horizontal-shadow-top"></div>
				
										
					
				</nav><!-- #access -->
			</div>
		</div><!-- #header-container -->
		<div id="ajax-container" class="control-height">
			<section id="content" role="main" data-title="About | Cultivated Wit" class="page-template page-template-page-about page-template-page-about-php page page-id-1408 clear control-height">


	


	<section id="about-us-main" class="container wrap clear">
		<h1 id="we-make-fun">We make guns fun!</h1>
		<div id="about-us-header-image" class="grid12">
			<img width="3751" height="1657" src="images/about.jpg" class="attachment-full size-full wp-post-image" alt="" srcset="images/about.jpg" 3751w />		</div>
		

		<div id="about-us-main-copy" class="grid6 float-grid-item">
			<h2 id="about-us-header">About Us</h2>
			<p>We serve as an illegal store-front, a middle-man between army supplies and the standard gun-toting civilian. We sell guns to anyone with money, no restrictions are in place. Currently payment is accepted only in bitcoin but will branch out to phones too.</p>
		</div>
		
		
		<div id="about-us-sub-copy" class="grid6 float-grid-item last-float-item">
			<div class="ipad-float-container float-grid-item ">
				<h3>Our Mission</h3>
				<div id="stories-copy" class="grid6">
					<p>We want 15 guns in every home by the end of 2045 to better protect us against alien invasions and the occasional hunting with grenade launchers. We believe that humor combined with slick design and a creative use of technology can make complicated ideas more understandable and products more fun, thus giving guns more coverage. Our mission is to bring great ideas to life and help others do the same, but with guns.</p>				</div>
			</div>
			
			<div class="ipad-float-container float-grid-item last-float-item" >
				<h3>Why apple.com?</h3>
				<div id="cultivated-wit-copy" class="grid6">
					<p>Why not? Where else are you getting your guns from?</p>				</div>
			</div>
			
		</div>
	</section>


		
			

			

			
			<div id="inspire-container" class="grid4 float-grid-item last-float-item">


				
			</div>
			
		 </div>

	 </section>
	<!-- clients-collaborators-container -->
	
	                                      
			</section><!-- #main -->
		</div><!-- #ajax-container -->
		<footer role="contentinfo" class="clear">
			<div id="footer-inner" class="container wrap clear">
				<div id="footer-communicate" class="footer-column grid4 float-grid-item ">
					<div class="footer-column-inner">
						<h3>COMMUNICATE <span class="force-break"></span>AT US</h3>
						<div id="email-name">
							<div id="footer-title-container">Email:</div>
							<a href="mailto:notarealemail@apple.com" title="Email Cultivated Wit">SteveJobs<br>@apple.com</a>
						</div>
					</div>
				</div>
			
				<div id="footer-social" class="footer-column grid4 float-grid-item ">
					
				</div>
			
				
			</div>
			

		</footer><!-- footer -->

		
			
		
		<script type='text/javascript' src='http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/themes/cultivatedwit/cultivatedwit-child/js/cultivatedwit.js'></script>		
	
	
		<script type='text/javascript' src='http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.49.0-2014.02.05'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"http:\/\/www.cultivatedwit.com\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Sending ...","cached":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=3.7.1'></script>
<script type='text/javascript' src='http://pvjvup4n51wq8m86.zippykid.netdna-cdn.com/wp-includes/js/wp-embed.min.js?ver=4.7.5'></script>
	  <script type="text/javascript">
		(function() {
		  var t   = document.createElement( 'script' );
		  t.type  = 'text/javascript';
		  t.async = true;
		  t.id    = 'gauges-tracker';
		  t.setAttribute( 'data-site-id', '5136bae5108d7b6b55000079' );
		  t.src = '//secure.gaug.es/track.js';
		  var s = document.getElementsByTagName( 'script' )[0];
		  s.parentNode.insertBefore( t, s );
		})();
	  </script>
	  
		
	</body>
</html>

