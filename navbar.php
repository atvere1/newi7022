<html>

<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="#">WebSiteName</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">Page 1</a></li>
        <li><a href="#">Page 2</a></li> 
        <li><a href="#">Page 3</a></li> 
      </ul>
      <ul class="nav navbar-nav navbar-right">
				<?php 
				if (array_key_exists("user", $_SESSION)) { ?>
					<li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>			
			<?php
			} else { ?>	
					<li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
	        <li></span><a href="#" data-toggle="modal" data-target="#loginformmodal"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
			<?php
			} ?>
        
      </ul>
    </div>
  </div>
</nav>
<div class="modal fade" id="loginformmodal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
           <form method="post" action="login.php">
  					<div class="form-group">
					    <label for="user">Username:</label>
					    <input type="text" class="form-control" name="user">
					  </div>
					  <div class="form-group">
					    <label for="password">Password:</label>
					    <input type="password" class="form-control" name="password">
					  </div>
					  	<input type="submit" class="btn btn-info" value="Login">
						</form> 
        </div>
      </div>
    </div>
  </div>
</html>
