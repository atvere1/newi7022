<?php

function template($str, $values) {
	foreach ($values as $from => $to) {
		$str = str_replace('{{' . $from . '}}', $to, $str);
	}
	return $str;
}
