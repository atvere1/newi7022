<?php
require_once("header.php");
require_once("database.php");

$query="insert into shop_users (
	email,
	`password`,
	first_name,
	last_name,
	phone,
	salutation,
	vatin,
	company,
	country,
	address	
) values (
    :email,
    password(:password),
    :first_name,
    :last_name,
    :phone,
    :salutation,
    :vatin,
    :company,
    :country,
    :address
)";

$statement = $db->prepare($query);

$statement->bindParam("email",$_POST["email"]);
$statement->bindParam("password",$_POST["password"]);
$statement->bindParam("first_name",$_POST["first_name"]);
$statement->bindParam("last_name",$_POST["last_name"]);
$statement->bindParam("phone",$_POST["phone"]);
$statement->bindParam("salutation",$_POST["salutation"]);
$statement->bindParam("vatin",$_POST["vatin"]);
$statement->bindParam("company",$_POST["company"]);
$statement->bindParam("country",$_POST["country"]);
$statement->bindParam("address",$_POST["address"]);


if ($statement->execute()) {
    echo "Registration was successful! <a href=\"index.php\">Back to main page</a>";
} else {
	var_dump($statement->errorInfo());
	die;
	
    if ($statement->errno == 1062) {

       echo "This e-mail is already registered";
    } else {

       die("Execute failed: (" .
           $statement->errno . ") " . $statement->error);
    }
}


?>
