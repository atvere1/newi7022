<?php
require_once("auth.php");
require_once("header.php");
require_once("database.php");
require_once("common.php");

$query="select * from product";
$statement = $db->prepare($query);
$statement->execute();

$data=$statement->fetchAll(PDO::FETCH_ASSOC); 

$table="";
$html =
   '<tr>' .
   '<td><a href="product.php?id={{id}}">{{name}}</a></td>' .
   '<td>{{price}}<span class="glyphicon glyphicon-bitcoin" aria-hidden="true"></span></p></td>' .
   '<td><img src="{{image}}" alt="{{alt}}"></td>' .
   '</tr>';
   
foreach ($data as $product){
	$table .= template($html, [
		'id' => $product['id'],
		'name' => $product['name'],
		'price' => $product['price'],
		'image' => $product['img'],
		'alt' => $product['alt']
		
	]);
}
?>



<div class="container">
	<h2>Products</h2>

	<table class="table">
		<tbody>
	  <tr>
		<th>Name</th>
		<th>Price</th>
		<th>Image</th>
	  </tr>
	  </tbody>
	<?=$table?>
	</table>
	
</div>

</body>
</html>
