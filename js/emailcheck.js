####http://stackoverflow.com/questions/46155/validate-email-address-in-javascript/38544440###

function validateEmail(email) {
    var chrbeforAt = email.substr(0, email.indexOf('@'));
    if (!($.trim(email).length > 127)) {
        if (chrbeforAt.length >= 2) {
            var re = /^(([^<>()[\]{}'^?\\.,!|//#%*-+=&;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            //var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
			return re.test(email);
		} else {
			return false;
		}
	} else {
		return false;
	}

}
######################################################################################

document.getElementById("emailcheck").addEventListener("submit",validate);

function validate(event) {
    var email = document.getElementById("email").value;
    if(validateEmail(email)){
	    return true
	} else {
	    alert("Email is invalid, please try again");
        event.preventDefault();
	}
		
     
    
}


