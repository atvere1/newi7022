<?php

require_once("header.php");
require_once("database.php");

$query="select * from shop_users where email = :email and `password` = PASSWORD(:password)"; 

$statement = $db->prepare($query);
$statement->bindParam("email",$_POST["email"]);
$statement->bindParam("password",$_POST["password"]);
$statement->execute();

$row=$statement->fetch(PDO::FETCH_ASSOC); 

if($row) {
    echo "Login successful, hello " . $row["first_name"];
    $_SESSION["user"] = $row["id"];
    $_SESSION["name"] = $row["first_name"];
     
} else {
    echo "Login failed";
}

header("Location: products.php");

