<!DOCTYPE html>
<html>
	<?php include('../includes/header.php'); 
				include('../config/conn.php');
				include('../includes/navbar-pages.php'); 
	?>
  <head>
    <meta charset="utf-8"/>
    <meta name="description" content="Introduction to this guy's website">
    <title>This goes into the titlebar</title>
    <link rel="css/style.css" type="text/css"/>
    <meta name="viewport" content="width=device-width"/><!-- Disable zoom on smartphone -->
  </head>
<?php
$conn = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
if ($conn->connect_error)
  die("Connection to database failed:" .
    $conn->connect_error);
$statement = $conn->prepare("SELECT `id`, `shipping_address`, `created` FROM `shop_order` WHERE `user_id` = ?");
$statement->bind_param("i", $_SESSION["user"]);
$statement->execute();
$results = $statement->get_result();
?>
  <body style="padding-top: 50px;">
		<?php checkLoginSuccess(); ?>
		<div class="jumbotron" style="margin-bottom:0;background:#83b819;">
			<div class="container" >
	  	  <h1>Andy's Autos</h1> 
 			  <p>Your one stop shop for every manufacturer on the market!</p>
			</div>
		</div>
		<div class="container"><h1>Orders</h1>
		<ul class="list-group">
<?php
while ($row = $results->fetch_assoc()) {
  ?>
    <li class="list-group-item">
		<a href="orderdetail.php?id=<?=$row['id']?>">        
		<?='Order #'.$row['id'];?>
    <?=$row['created'];?>
    </li> 
<?php } ?>
		</div>
		</ul>
  </body>
</html>

<?php
