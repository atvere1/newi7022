<?php
require_once("auth.php");


require_once("header.php");
require_once("common.php");
require_once("database.php");

$query="select * from product where id=:id";

$statement = $db->prepare($query);
$statement->bindParam("id",$_GET["id"]);
$statement->execute();

$data=$statement->fetch(PDO::FETCH_ASSOC); 




?>



	
<div class="container">
	<div class="col-md-4">
		<img src="<?=$data['img']?>" alt="{{alt}}">
		<p><?=$data["description"]?></p>
	</div>
	<div class="col-md-4">
		<h2><?=$data["name"]?></h2>
		<p><?=$data["price"]?><span class="glyphicon glyphicon-bitcoin" aria-hidden="true"></span></p>
	</div>
	<div class="col-md-4">
		<form method="post" action="buy.php">
			<div class="form-group">
		<label for="selection">Quantity:</label>
		<select name="quantity" class="form-control" style="width:200px">
			<option>1</option>
			<option>2</option>
			<option>3</option>
			<option>4</option>
		</select>
	</div>
</div>
<input type="hidden" name="id" value="<?=$_GET["id"]?>"/>
<button type="submit" class="btn btn-primary">Add to cart</button>
</form>
</div>

<!-- <a href="buy.php?id=<?#=$_GET["id"]?>"><span id="BuyButton" class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span></a>



